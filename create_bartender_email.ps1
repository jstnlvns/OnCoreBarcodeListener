Function Send-EMail {
    Param (
        [Parameter(`
            Mandatory=$true)]
        [String]$EmailTo,
        [Parameter(`
            Mandatory=$true)]
        [String]$Subject,
        [Parameter(`
            Mandatory=$true)]
        [String]$Body
    )

        $EmailFrom = "[[EMAIL FROM ADDRESS]]"
        $SMTPServer = "[[SMTP SERVER TO SEND FROM]]" #this may not be available for everyone
        $SMTPMessage = New-Object System.Net.Mail.MailMessage($EmailFrom,$EmailTo,$Subject,$Body)
        $SMTPClient = New-Object Net.Mail.SmtpClient($SmtpServer, 25)
        $SMTPClient.Send($SMTPMessage)
        Remove-Variable -Name SMTPClient

} #End Function Send-EMail

Send-EMail -EmailTo "[[EMAIL SEND TO ADDRESS]]" -Body "[[EMAIL BODY]]" -Subject "[[EMAIL SUBJECT]]"
