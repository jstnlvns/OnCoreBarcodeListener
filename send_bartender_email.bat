
@echo off
setlocal EnableDelayedExpansion

REM Copy log file from forte directory to script directory
xcopy /Y "C:\forte\barcode-listener\barcode-listener.log" "C:\forte\scripts\"

REM Looks at the last line in log file and stores value as results
FOR /F "usebackq tokens=3 delims= " %%i in (C:\forte\scripts\barcode-listener.log) do (
	set "previous=!last!"
	set "last=%%i"
)
SET RESULT=!previous!

REM if results = ERROR then kill bartend.exe (this has to be done to perform a few of the actions below), clear out the temporary directory, stop and restart the listener
REM #lastly, send an email
IF "%RESULT%" == "ERROR" (
	TASKKILL /F /IM bartend.exe
	cd "C:\forte\barcode-listener\temp"
	del /F /Q *.txt
	REM Stop and restart listener (if the name of your service is differenct just change the "BSM Barcode Listener")
	net stop "BSM Barcode Listener"
	net start "BSM Barcode Listener"
	C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy ByPass -File C:\forte\scripts\create_bartender_email.ps1
)