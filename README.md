`create_bartender_email.ps1` - This is a MS Powershell script that is called by `send_bartender_email.bat` when an error is found in the log.

```
Function Send-EMail {
    Param (
        [Parameter(`
            Mandatory=$true)]
        [String]$EmailTo,
        [Parameter(`
            Mandatory=$true)]
        [String]$Subject,
        [Parameter(`
            Mandatory=$true)]
        [String]$Body
    )

        $EmailFrom = "[[EMAIL FROM ADDRESS]]"
        $SMTPServer = "[[SMTP SERVER TO SEND FROM]]" # this may not be available for everyone
        $SMTPMessage = New-Object System.Net.Mail.MailMessage($EmailFrom,$EmailTo,$Subject,$Body)
        $SMTPClient = New-Object Net.Mail.SmtpClient($SmtpServer, 25)
        $SMTPClient.Send($SMTPMessage)
        Remove-Variable -Name SMTPClient

} #End Function Send-EMail

Send-EMail -EmailTo "[[EMAIL SEND TO ADDRESS]]" -Body "[[EMAIL BODY]]" -Subject "[[EMAIL SUBJECT]]"
```

`send_bardenter_email.bat` - This is a Windows Batch file that does a few things

```
@echo off
setlocal EnableDelayedExpansion

REM Copy log file from forte directory to script directory
xcopy /Y "C:\forte\barcode-listener\barcode-listener.log" "C:\forte\scripts\"

REM Looks at the last line in log file and stores value as results
FOR /F "usebackq tokens=3 delims= " %%i in (C:\forte\scripts\barcode-listener.log) do (
	set "previous=!last!"
	set "last=%%i"
)
SET RESULT=!previous!

REM if results = ERROR then kill bartend.exe (this has to be done to perform a few of the actions below), clear out the temporary directory, stop and restart the listener
REM #lastly, send an email
IF "%RESULT%" == "ERROR" (
	TASKKILL /F /IM bartend.exe
	cd "C:\forte\barcode-listener\temp"
	del /F /Q *.txt
	REM Stop and restart listener (if the name of your service is differenct just change the "BSM Barcode Listener")
	net stop "BSM Barcode Listener"
	net start "BSM Barcode Listener"
	C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy ByPass -File C:\forte\scripts\create_bartender_email.ps1
)
```

To run the script automatically, you'll need to set up a scheduled task on the server that runs at what ever frequency you want (i.e. every 5 minutes). The task should be set up to start a program which would be `send_bardenter_email.bat`. Depending on where your listener is installed (mine is in the root of the C drive), you may need elevated permissions to run the batch file, so make sure what ever user you select to run the script on the task has appropriate permissions.

General tab of scheduled task

<img src="General.png" alt="General" height="500px">

Actions tab of scheduled task

<img src="Actions.png" alt="Actions" height="500px">